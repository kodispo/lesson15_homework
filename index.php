<?php
$search_words = ['php', 'html', 'интернет', 'Web'];

$search_strings = [
    'Интернет - большая сеть компьютеров, которые могут взаимодействовать друг с другом.',
    'PHP - это распространенный язык программирования с открытым исходным кодом.',
    'PHP сконструирован специально для ведения Web-разработок и его код может внедряться непосредственно в HTML'
];

function searchWords(array $searchWords, array $targetStrings) {
    if (empty($searchWords) || empty($targetStrings)) return;

    $regExp = '/(' . implode('|', $searchWords) . ')/iu';

    foreach ($targetStrings as $key => $string) {
        if (preg_match_all($regExp, $string, $matches)) {
            $num = $key + 1;
            echo 'В предложении №' . $num . ' есть слова: ' . implode( ', ', array_unique($matches[0]) ) . '.<br>';
        }
    }
}

searchWords($search_words, $search_strings);